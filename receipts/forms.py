from django import forms
from receipts.models import Receipt, ExpenseCategory, Account


# Feature 11, step 1
class ReceiptForm(forms.ModelForm):
    class Meta:
        model = Receipt
        fields = (
            "vendor",
            "total",
            "tax",
            "date",
            "category",
            "account",
        )


# Feature 12
class CategoryForm(forms.ModelForm):
    class Meta:
        model = ExpenseCategory
        fields = ("name",)


# Feature 12
class AccountForm(forms.ModelForm):
    class Meta:
        model = Account
        fields = (
            "name",
            "number",
        )
