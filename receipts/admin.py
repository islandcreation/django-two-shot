from django.contrib import admin
from receipts.models import ExpenseCategory, Account, Receipt


# Register ExpenseCategory model with the admin site
@admin.register(ExpenseCategory)
class ExpenseCategoryAdmin(admin.ModelAdmin):
    pass


# Register Account model with the admin site
@admin.register(Account)
class AccountAdmin(admin.ModelAdmin):
    pass


# Register Receipt model with the admin site
@admin.register(Receipt)
class ReceiptAdmin(admin.ModelAdmin):
    pass
